var viewmodel = {
	stringValue: ko.observable("hello"),
	passwordValue: ko.observable("mypass"),
	booleanValue: ko.observable(true),
	optionsValue: ["alpha","beta","gamma"] ,
	selectedOptionValue: ko.observable("alpha"),
	multipleSelectedOptionValue: ko.observable(['beta']),
	radioButtonOptionValue: ko.observable('gamma')
};
ko.applyBindings(viewmodel);