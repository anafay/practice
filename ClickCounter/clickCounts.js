var clickCounterViewModel = function (){
	this.numberOfClicks = ko.observable(0);

	this.registerClick = function(){
		return this.numberOfClicks(this.numberOfClicks() + 1);
	};
	this.resetClick = function(){
		return this.numberOfClicks(0);
	};
	this.hasClickedTooManyTimes = ko.computed(function()
	{
		return this.numberOfClicks() > 3;
	},this);
};
//ko.applyBindings(new clickCounterViewModel());