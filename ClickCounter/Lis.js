var SimpleListViewModel = function(){
	this.allItems = ko.observableArray(["egg","spinach","chicken","McChicken","OreoShake"]);
	this.itemToAdd = ko.observable("");
	this.selectedItem = ko.observable(["chicken"]);
	this.addItem = function(){
		if(this.itemToAdd() != "" && (this.allItems.indexOf(this.itemToAdd()) < 0)) 
		{
			this.allItems.push(this.itemToAdd());
		this.itemToAdd("");
	}
	};
	this.removeItem = function() {
		this.allItems.removeAll(this.selectedItem());
		this.selectedItem([]);
	};
	this.sortItems = function(){
		this.allItems.sort();
	};
};
ko.applyBindings(new SimpleListViewModel());